using DefaultNamespace.UI;
using UnityEngine;

namespace DefaultNamespace {
	public class GameManager : MonoBehaviour {
		[SerializeField] private UIManager uiManager;
		[SerializeField] private ResourceLoader resourceLoader;
		private int levelIndex;

		private void OnEnable() {
			uiManager.OnAnswer += OnAnswer;
		}

		private void OnDisable() {
			uiManager.OnAnswer -= OnAnswer;
		}

		private void Start() {
			resourceLoader.LoadQuestions();
			StartGame();
		}

		private void StartGame() {
			levelIndex = 0;
			selectQuestion();
		}

		private void selectQuestion() {
			var question = resourceLoader.Questions[levelIndex];
			uiManager.SetQuestion(question);
		}

		private void NextLevel() {
			levelIndex++;
			if (levelIndex > resourceLoader.Questions.Count) {
				GameWin();
				return;
			}

			selectQuestion();
		}

		private void GameWin() {
		}

		private void GameOver() {
			levelIndex = 0;
			Debug.LogError($"Game Over, Your score: {levelIndex + 1}");
		}
		
		private void OnAnswer(bool isCorrect) {
			if (isCorrect) {
				NextLevel();
				return;
			}
			GameOver();
		}
	}
}