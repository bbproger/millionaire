using System;
using System.Linq;
using UnityEngine;

namespace DefaultNamespace.UI {
	public delegate void OnAnswerDelegate(bool isCorrect);

	public class GameUIPageController : MonoBehaviour {
		public event OnAnswerDelegate OnAnswer;
		[SerializeField] private GameUIPage gameUiPage;
		private Question question;


		public void SetQuestion(Question question) {
			this.question = question;
			gameUiPage.SetQuestionText(question);
			SetVariants();
		}

		private void SetVariants() {
			var selectedVariants = question.Variants.OrderBy(s => Guid.NewGuid()).Take(3).ToList();
			selectedVariants.Add(question.Answer);
			selectedVariants = selectedVariants.OrderBy(s => Guid.NewGuid()).ToList();
			gameUiPage.SetVariants(selectedVariants.ToArray());
		}

		public void OnVariantSelected(string variant) {
			OnAnswer?.Invoke(question.IsCorrect(variant));
		}
	}
}