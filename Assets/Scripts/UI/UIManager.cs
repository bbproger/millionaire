using UnityEngine;

namespace DefaultNamespace.UI {
	public class UIManager : MonoBehaviour {
		[SerializeField] private GameUIPageController gameUiPageController;

		public event OnAnswerDelegate OnAnswer {
			add => gameUiPageController.OnAnswer += value;
			remove => gameUiPageController.OnAnswer -= value;
		}


		public void SetQuestion(Question question) {
			gameUiPageController.SetQuestion(question);
		}
	}
}