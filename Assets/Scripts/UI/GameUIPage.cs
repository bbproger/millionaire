using System;
using System.Collections.Generic;
using DefaultNamespace.UI.Component;
using TMPro;
using UnityEngine;

namespace DefaultNamespace.UI {
	public class GameUIPage : MonoBehaviour {
		[SerializeField] private TextMeshProUGUI questionText;
		[SerializeField] private List<VariantItem> variantItems;
		[SerializeField] private GameUIPageController controller;


		private void OnEnable() {
			variantItems.ForEach(item => { item.OnVariantSelected += controller.OnVariantSelected; });
		}

		private void OnDisable() {
			variantItems.ForEach(item => { item.OnVariantSelected -= controller.OnVariantSelected; });
		}

		public void SetQuestionText(Question question) {
			questionText.text = question.Questions;
		}

		public void SetVariants(string[] variants) {
			for (var i = 0; i < variantItems.Count; i++) {
				variantItems[i].SetVariant(variants[i]);
			}
		}
	}
}