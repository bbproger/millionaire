using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.UI.Component {
	public class VariantItem : MonoBehaviour {
		public delegate void OnVariantSelectedDelegate(string variant);

		public event OnVariantSelectedDelegate OnVariantSelected;
		
		[SerializeField] private TextMeshProUGUI variantText;
		[SerializeField] private Button variantButton;

		private string variant;

		private void OnEnable() {
			variantButton.onClick.AddListener(OnVariantButtonClick);
		}

		private void OnDisable() {
			variantButton.onClick.RemoveListener(OnVariantButtonClick);
		}

		private void OnVariantButtonClick() {
			OnVariantSelected?.Invoke(variant);
		}

		public void SetVariant(string variant) {
			this.variant = variant;
			variantText.text = variant;
		}
	}
}