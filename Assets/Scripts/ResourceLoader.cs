using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace DefaultNamespace {
	public class ResourceLoader : MonoBehaviour {
		private const string DATA_PATH = "Data/data";

		public List<Question> Questions { get; private set; }

		public void LoadQuestions() {
			var questionJson = Resources.Load<TextAsset>(DATA_PATH);
			Questions = JsonConvert.DeserializeObject<List<Question>>(questionJson.text);
		}
	}
}