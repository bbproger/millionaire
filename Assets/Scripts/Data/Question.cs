﻿using System.Collections.Generic;

public class Question {
	public string Questions { get; set; }
	public string Answer { get; set; }
	public List<string> Variants { get; set; }
	public int Difficulty { get; set; }

	public bool IsCorrect(string variant) {
		return variant == Answer;
	}
}